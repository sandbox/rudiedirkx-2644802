var fs = require('fs');
var rwebsocket = require('./rwebsocket.js');

function _log(msg) {
  console.log.apply(console, arguments);
  console.log('');
}

var commands = {
  _open: function() {
    this.sendCmd('welcome', {id: this.data.id});
  },
  insert: function(data) {
    this.withAllOtherClients(function(client) {
      client.sendCmd('msg', {msg: this.data.id + ' INSERTED "' + data.title + '"'});
    });
  },
  update: function(data) {
    this.withAllOtherClients(function(client) {
      client.sendCmd('msg', {msg: this.data.id + ' UPDATED "' + data.title + '"'});
    });
  },
};

var cfg = require('./wss-conf');
var options = {
  port: cfg.port,
  ssl : cfg.ssl,
  commands: commands,
};
var rws = rwebsocket(options);
