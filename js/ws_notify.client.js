(function($) {

  WebSocket.prototype.on = WebSocket.prototype.addEventListener;

  $(function() {

    var url = Drupal.settings.ws_notify.url; // 'wss://' + location.hostname + ':' + port;
    var socket = new WebSocket(url);
    socket.on('open', function() {
      this.connectedAt = new Date;
      document.body.classList.add('ws-notify-connected');
    });
    socket.on('message', function(e) {
      var msg = JSON.parse(e.data);
console.log(msg.cmd, msg);

      if (msg.cmd == 'msg') {
        var $console = $('<div id="messages" class="messages warning"><ul><li></li></ul></div>');
        $console.find('li').text(msg.msg);
        $console.insertAfter('#header, #page > .tabs-secondary');
      }
    });
    socket.on('close', function() {
      document.body.classList.remove('ws-notify-connected');
    });

  }); // domready

})(jQuery);
